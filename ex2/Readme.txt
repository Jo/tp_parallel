ATTENTION:
Le programme ne termine pas malgré un affichage correct. 
Je n'ai pas d'erreur ni de warning et j'ai vérifié et debugué mon code plusieurs fois ligne par ligne. 

 
Compilation:
	make ex2
Exécution:
	mpirun -np 2 ./a.out


Dans ce programme, le but est d'utiliser la fonction Reduce_Scatter pour effectuer un produit matrice vecteur AX = Y.


-- Etape 1:
Initialiser MPI, la matrice A, le vecteur X

-- Etape 2
Communiquer A à partir du processus master à tous les autres processus du communicateur en Broadcast. 

-- Etape 3
Diviser et partager le vecteur X entre tous les processus du communicateur grâce à la fonction Scatter.
Cela implique que le partage est équitable. Il faut donc que la taille du vecteur X soit divisible par le nombre de processus.

-- Etape 4
On alloue un vecteur temporaire dans chaque processus de la taille du vecteur Y, qui contient une décomposition de la somme des termes qui constituent le produit. ( A[i,k]*X[k] + A[i, k+1]*X[k+1] + ...). Ainsi on effectue les produits des sous-matrices de A avec les sous-vecteurs de X correspondants.


-- Etape 5
Le Reduce_scatter, à partir de la décomposition de l'étape 4, regroupe ces "sous-produits" pour obtenir le produit final dans le vecteur Y.

-- Etape 6
Vérification du résultat.



