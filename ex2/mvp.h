
/*! \file mvp.h
 *  \author Joseph Bexiga <bexigajose@eisti.eu>
 *  \version 0.1
 *  \date Dimanche 5 Avril 2019
 *  \brief Produit matrice-vecteur en parallèle MPI
 */

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include "../functions.c"

