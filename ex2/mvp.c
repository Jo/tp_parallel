
#include "mvp.h"

#define MASTER 0               /* taskid of first task */
#define TOL  0.001

int main(int argc, char *argv[])
{
	int M, N;   /* A[M][N], X[N], Y[N] */
	M = 5;
	N = 4;
	int i,j;
    int myid, np;
	double A[M][N]; //Matrice A
	double *X;		//Vecteur X
	double *Y;		//Vecteur Y résultat de A * X
	double *Yt;
	double aval,xval, yval;
	double err, errsq;
	int lpp; //Lignes par processus
	int s;

	//------------------ Initialisation MPI --------------------- //
	
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &np);
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);
    

	if(myid == 0){
		printf("Le programme s'est lancé avec %d processus \n", np);
	}
    
	
	aval = 3.0;
	xval = 2.0;

	lpp = N/np;
	Y 	= malloc(N * sizeof(double)); //Vecteur résultat

	//------------------ Initialisation Matrice A et vecteur X --------------------- //
	if (myid == 0){
		for (i=0; i<M; i++){
       		for (j=0; j<N; j++){
    			A[i][j]= aval;
			}
		}

		X = malloc(N*sizeof(double));

		for (i=0; i<N; i++){
				X[i]= xval;
		}
		
	}

	if (myid > 0){
		X = malloc(lpp * sizeof(double));
	}

	//-------------------- A est communiqué entièrement à tous les processus --------------------//
	MPI_Bcast(A, M*N, MPI_DOUBLE, 0, MPI_COMM_WORLD);

	
	//-------------------- X est divisé et partagé entre tous les processus --------------------//
	MPI_Scatter(X, lpp, MPI_DOUBLE, X, lpp, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	
	
	//Un vecteur temporaire est alloué dans chaque processus
	Yt = malloc(M * sizeof(double));

	for (int i = 0; i < M; i++){
        s = 0;
        for (int j = 0; j < lpp; j++){
			s += A[i][myid * lpp +j] * X[j];
        }
		Yt[i] = s;
	}

	//------------------------- Reduce scatter ------------------------------//
	MPI_Reduce_scatter(Yt, Y, &M, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	
	
	//Affichage du résultat final
	printf("Rank %d, Y = \n", myid);
	for(int i = 0; i < M; i++) {
		printf("%6.2f\n", Y[i]);
	}
	/* Check results */
	yval = aval*xval*(double)N;	
	errsq = 0.0; 
	for (i=0; i<M; i++){
		err = Y[i] - yval; 
		errsq += err * err;
	}
	
	if (errsq > TOL) {
		printf("\n Errors in multiplication: %f\n",errsq);
	}
	else{
		printf("\n The result is correct");
	}
	MPI_Finalize();

	return 0;
}
	

/* 
mpicc -Wall -Wextra mvp.c
mpirun -np 4 ./a.out
*/