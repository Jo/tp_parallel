Compilation:
	make ex3
		ou
	mpicc -Wall -Wextra mpimm.c
Exécution:
	mpirun -np 4 ./a.out


Dans ce programme, le but est d'utiliser des fonctions collectives de MPI pour effectuer le produit de deux matrices. 

-- Etape 1:
Initialisation de matrice carrées pour simplifier les opérations.
Initialisation de MPI (size, rank..) 

-- Etape 2
Division de la matrice A en nbe parties égales et répartition entre les processus du communicateur (Scatter)

-- Etape 3
Broadcast de la matrice B. 

-- Etape 4
Produit de chaque sous-matrice Ai avec la partie de B correspondante. 

-- Etape 5
Regroupement des sous-matrices réparties dans un seul processus (Gather)

-- Etape 6
Vérification du résultat
Pour cela on peut utiliser vérifier que le produit de A vec la matrice identité donne bien A. 
La matrice identité I4:
{{1,0,0,0},{0,1,0,0},{0,0,1,0},{0,0,0,1,}};


Explication:
Ce découpage des matrices pour le produit de matrices a été vu en cours, avec des opérations non collectives: MPI_Send et MPI_Recv. 


