
#include "mpimm.h"


//------------------- Affichage matrice----------------//
void printMatrix(int a[N][N])
{
    int i, j;
    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
			if (a[i][j] <= 10)
            	printf("%d  ", a[i][j]);
			else
			 printf("%d ", a[i][j]);
        }
        printf ("\n");
    }
    printf ("\n");
}

int main(int argc, char *argv[])
{
    int i, j, myid, np, s = 0;
	//------------------- Initialisation des matrices ----------------//
    int a[N][N] = {{1,2,3,4},{5,6,7,8},{9,1,2,3},{4,5,6,7,}};
    int b[N][N] ={{2,2,2,2},{2,2,2,2},{2,2,2,2},{2,2,2,2,}};
	//{{1,0,0,0},{0,1,0,0},{0,0,1,0},{0,0,0,1,}};
    int c[N][N];
    int chunk[N],vres[N];

    //------------------- Initialisation de MPI ----------------//
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &np);
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);
	MPI_Barrier(MPI_COMM_WORLD);

    //Affichage matrices initiales
	if(myid ==0){
		printf("A =\n");
		printMatrix(a);
		printf("B =\n");
		printMatrix(b);
	}
	
	int nbe = (N*N)/np; //Taille de la sous-matrice à envoyer aux processus


    //------------------- Division et partage de la matrice A entre les processus ----------------//
    MPI_Scatter(a, nbe, MPI_INT, chunk, nbe, MPI_INT,0,MPI_COMM_WORLD); 

    //------------------- Broadcast de la matrice B ----------------//
    MPI_Bcast(b, N*N, MPI_INT, 0, MPI_COMM_WORLD);

    MPI_Barrier(MPI_COMM_WORLD);

    //------------------- Produit des sous-matrices de A avec les sous-matrices de B correspondantes ----------------//
    for (i = 0; i < N; i++)
    {
            for (j = 0; j < N; j++)
            {
                    s = s + chunk[j] * b[j][i];                
            }
            vres[i] = s;
            s = 0;
    }
    //------------------- Regrouppement des sous-produits dans le processus 0 ----------------//
    MPI_Gather(vres, nbe, MPI_INT, c, nbe, MPI_INT, 0, MPI_COMM_WORLD);

    MPI_Barrier(MPI_COMM_WORLD);        
    MPI_Finalize();
    
    //Affichage
    if (myid == 0)              
        printMatrix(c);
}

