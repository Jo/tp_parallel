/* 
Écrire une implémentation OpenMP de l’algorithme de tri fusion récursive en utilisant les
directives Task. Le code doit trier correctement un tableau de taille N.
*/

/*! \file triFusion_par.h
 *  \author Joseph Bexiga <bexigajose@eisti.eu>
 *  \version 0.1
 *  \date Dimanche 5 Avril 2020
 *  \brief Produit de deux matrices carrées en parallèle MPI
 */
#include <stdio.h>
#include <math.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stddef.h>
#include <mpi.h>

#define N 4


/*! 
 * \fn void printMatrix(int a[N][N]);
 * \author Joseph Bexiga <bexigajose@eisti.eu>
 * \version 0.1
 * \date Dimanche 5 Avril 2020
 * \brief Affiche une matrice carrée d'entiers
 * 
 * \param a   : matrice carrée
 * \remarks 
 */
void printMatrix(int a[N][N]);
