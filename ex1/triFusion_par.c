
#include "triFusion_par.h"


void fusion(int res[], int g, int milieu, int d){
    int i, j, k;
    int m, n; 

    m = milieu-g+1;
    n = d-milieu;
    
    int a[m], b[n];

    for (int i= 0; i<m; i++){
        a[i] = res[g+i];
    }

    for (int i= 0; i<n; i++){
        b[i] = res[milieu+1+i];
    }

    i = 0;
    j = 0;
    k = g;


    //Condition d'arrêt
    while ((i < m && j < n))
    {
        if (a[i] <= b[j]){
            res[k] = a[i];
            i++;
            }
        else
        {
            res[k] = b[j];
            j++;
        }
        k++;
    }
    //Si un tableau a fini d'être parcouru, on copie le reste du deuxième tableau
    while (i < m){
        res[k] = a[i];
        i++;
        k++;
    }

    while (j < n){
        res[k] = b[j];
        j++;
        k++;
    }

}

void triFusion(int v[], int g, int d){
    //printf("%d.", omp_get_thread_num());
    if (g < d)
    {
        int milieu = g + (d-g)/2;
        #pragma omp task if(d-g >= 2000)
        triFusion(v, g, milieu);

        #pragma omp task if(d-g >= 2000)
        triFusion(v, milieu+1, d);

        #pragma omp taskwait
        fusion(v, g, milieu, d);
    }
}



int main(int argc, char *argv[]){
    double start_time, run_time;
    //Taille du tableau en paramètre 
    int n   = atoi(argv[1]);
    int nth = atoi(argv[2]);
    int v[n];
    
    omp_set_num_threads(nth);

    //Création et initialisation aléatoire d'un vecteur d'entiers de taille n
    srand(time(NULL));
    for(int i=0; i<n; i++){
        v[i] = rand()%100 +1;
    }

	start_time  = omp_get_wtime();  //Start measuring the execution time here 
    #pragma omp parallel 
    {
        #pragma omp single
        triFusion(v, 0, n-1);
    }
	run_time    = (omp_get_wtime() - start_time); //Stop measuring the execution time here
    
    printf("Run time en parallèle: %lf\n", run_time);


    //Vérification du résultat
    int check = 1;
    int j = 0;
    while(check && j<n-1)
    {
        if (v[j] > v[j+1])
            check = 0;
        j++;
    }
    if (check)
        printf("Le résultat est correct\n");
    else
        printf("Résultat faux\n");

    return 0;
}


//gcc -fopenmp -Wall -Wextra triFusion_par.c