/* 
Écrire une implémentation OpenMP de l’algorithme de tri fusion récursive en utilisant les
directives Task. Le code doit trier correctement un tableau de taille N.
*/

/*! \file triFusion_par.h
 *  \author Joseph Bexiga <bexigajose@eisti.eu>
 *  \version 0.1
 *  \date Dimanche 5 Avril 2019
 *  \brief Algo tri fusion en parallèle
 */
#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include<string.h>
#include <time.h>
#include <omp.h>
#include "../functions.c"


/*! 
 * \fn fusion(int res[], int g, int milieu, int d) 
 * \author Joseph Bexiga <bexigajose@eisti.eu>
 * \version 0.1
 * \date Dimanche 5 Avril 2019
 *
 * \brief Fusionne deux sous-tableaux déjà triés le tableau res.
 * 
 * \param res   : tableaux initial, composé de deux sous-tableaux déjà triés
 * \param g     : indice du début du tableau
 * \param d     : indice fin de tableau
 * \param milieu: indice milieu du tableau
 * \remarks 
 */
void fusion(int res[], int g, int milieu, int d);


/*! 
 * \fn triFusion(int v[], int g, int d)
 * \author Joseph Bexiga <bexigajose@eisti.eu>
 * \version 0.1
 * \date Dimanche 5 Avril 2019
 *
 * \brief Fusionne deux sous-tableaux déjà triés le tableau res.
 * 
 * \param v : tableaux initial
 */
void triFusion(int v[], int g, int d);