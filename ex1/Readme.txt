Compilation: 
	make ex1
	
Exécution (Paramètre 1 = taille tableau, paramètre 2 = nombre de threads):
	./ex1 10 4
	./ex1 1000000 16
	make clean

L'objectif de ce programme est de faire un tri fusion récursive en utilisant les
directives Task de OpenMP. 
	

-- Etape 1:
Création et initialisation aléatoire d'un vecteur d'entiers de taille n. Ici l'utilistion de l'aléatoire se justifie par la taille élevée du tableau. En effet le programme fonctionne jusqu'à une taille de 1.5 million environ. Au delà, core dumped..

-- Etape 2
Appel de la fonction triFusion(), fonction récursive qui découpe le tableau en deux parties plus ou moins égales. Récursion sur chacun des deux sous-tableaux et fusionne le résultat.

-- Etape 3
La fonction fusion() est itérative. Elle permet de fusionner deux sous-tableaux déjà triés ett contenus dans un même tableau. 
Comme les tableaux sont déjà triés, il suffit de trouver le minimum entre le premier élément de chaque tableau.
Si un tableau a fini d'être parcouru, on copie le reste du deuxième tableau

-- Etape 4
Vérification du résultat. Etant donné que le tableau est aléatoire, on vérifie simplement que v[i] <= v[i+1] pour pour i = 0..n 


-- Etape 5
Parallélisation:
- single pour l'appel initial (1 seul appel)
- task et taskwait dans la fonction récursive SI n >= 2000. En effet la parallélisation est complètement inéfficace sur des petits tableaux. Après plusieurs tests, j'ai obtenu un meilleurs temps d'exécution avec n <= 2000
- J'ai également essayé de faire une parallélisation imbriquée, mais cela est beaucoup moins efficace. 


-- Etape 6
Moyenne des temps obtenus avec un tableau aléatoire de taille 1 million:
En séquentiel			: 0.19s	 
En parallèle avec 64 threads	: 0.09s
En parallèle, le programme est 2 fois plus rapide. 



