#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <omp.h>

/*
Fonction qui permet d'afficher un tableau d'entiers
*/
void printArray(int * a, int n){
    printf("{");
    for(int i=0; i<n; i++)
        printf("%d ", a[i]);
    printf("}\n");
}

/*
Fonction qui permet d'afficher un tableau d'entiers
*/
void printArrayd(double * a, int n){
    printf("{");
    for(int i=0; i<n; i++)
        printf("%6.2f ", a[i]);
    printf("}\n");
}

//Fonction qui permet de concaténer 2 tableaux d'entiers.
int * concat(int * a, int * b, int m, int n, int * res){
    memcpy(res, a, m* sizeof(int)); //Copie de m entiers de a dans res à partir de res.
    memcpy(res+m, b, n*sizeof(int)); //copie de n entiers de b dans res à partir de res+m 
    return res;
}


void printMatrix(int M, int N, int A[M][N]){
    for (int i = 0; i < M; i++){
        printf("\n");
        for(int j = 0; j<N; j++)
            printf("%d ", A[i][j]);
    }
}




/*
gcc -Wall -Wextra functions.c
*/